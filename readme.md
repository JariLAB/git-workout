# Unit converter

This README provides an overview of the project.

Key features:
1. User-friendly interface
2. Interesting unit converter
    - meters to centimeters
    - centimeters to meters
